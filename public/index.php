<?php

require __DIR__ . '/../vendor/autoload.php';

$app = new \Slim\App();

$app->get('/users', function ($request, $response, $args) {
    //Include static variables
    require __DIR__ . '/../src/variables.php';
    if ($request->hasHeader('Authorization')) {
        $header = $request->getHeaderLine('Authorization');
        $jwtToken = substr($header, strpos($header, "Bearer") + 7);
        $jwtParts = explode('.', $jwtToken);
        $jwtINeed = base64_decode($jwtParts[1], true);
        $jwtINeed = json_decode($jwtINeed, true);
        $myUser = $jwtINeed['upn'];
        $allAddresses = [];
        $allPhones = [];
        $allEmails = [];
        try {
            $userArray = getApi($userApi . $myUser . '/null/null/null/NAME,ABOUT,MAJORS', $basicPassword);
            $userPidm = $userArray["PIDM"];

            $personalInfo = [
                "LUID" => $userArray["ABOUT"]["LUID"],
                "PHOTOURL" => $userArray["ABOUT"]["PHOTOURL"],
                "NAME" => $userArray["NAME"],
                "MAJORS" => $userArray["MAJORS"],
            ];

            $FinalResponse["PersonalInfo"] = $personalInfo;

            $addressesArray = getApi($baseUrl . 'address/activeAddressRows/' . $userPidm, $basicPassword);

            foreach ($addressesArray as $address) {
                $currentAddress = [
                    "atypCode" => $address["SpraddrBean"]["addressDesc"],
                    "addressDesc" => $address["SpraddrBean"]["addressDesc"],
                    "houseNumber" => $address["SpraddrBean"]["houseNumber"],
                    "city" => $address["SpraddrBean"]["city"],
                    "cntyCode" => $address["SpraddrBean"]["cntyCode"],
                    "nationDesc" => $address["SpraddrBean"]["nationDesc"],
                    "natnCode" => $address["SpraddrBean"]["natnCode"],
                    "seqno" => $address["SpraddrBean"]["seqno"],
                    "statCode" => $address["SpraddrBean"]["statCode"],
                    "statDesc" => $address["SpraddrBean"]["statDesc"],
                    "streetLine1" => $address["SpraddrBean"]["streetLine1"],
                    "streetLine2" => $address["SpraddrBean"]["streetLine2"],
                    "streetLine3" => $address["SpraddrBean"]["streetLine3"],
                    "streetLine4" => $address["SpraddrBean"]["streetLine4"],
                    "zip" => $address["SpraddrBean"]["zip"],
                    "orig_seqno" => $address["SpraddrBean"]["orig_seqno"],
                    "orig_atypCode" => $address["SpraddrBean"]["orig_atypCode"],
                ];

                array_push($allAddresses, $currentAddress);
            }

            $FinalResponse["Addresses"] = $allAddresses;

            $phonesArray = getApi($baseUrl . 'phone/activePhoneRows/' . $userPidm, $basicPassword);

            foreach ($phonesArray as $phone) {
                $currentPhone = [
                    "atypDesc" => $phone["SprteleBean"]["atypDesc"],
                    "intlAccess" => $phone["SprteleBean"]["intlAccess"],
                    "ctryCodePhone" => $phone["SprteleBean"]["ctryCodePhone"],
                    "phoneArea" => $phone["SprteleBean"]["phoneArea"],
                    "phoneExt" => $phone["SprteleBean"]["phoneExt"],
                    "phoneNumber" => $phone["SprteleBean"]["phoneNumber"],
                    "seqno" => $phone["SprteleBean"]["seqno"],
                    "teleCode" => $phone["SprteleBean"]["teleCode"],
                    "primaryInd" => $phone["SprteleBean"]["primaryInd"],
                    "unlistInd" => $phone["SprteleBean"]["unlistInd"],
                    "comment" => $phone["SprteleBean"]["comment"],
                    "orig_seqno" => $phone["SprteleBean"]["orig_seqno"],
                    "orig_teleCode" => $phone["SprteleBean"]["orig_teleCode"],
                ];

                array_push($allPhones, $currentPhone);
            }

            $FinalResponse["Phones"] = $allPhones;

            $emailsArray = getApi($baseUrl . 'email/activeEmailRows/' . $userPidm, $basicPassword);

            foreach ($emailsArray as $email) {
                $currentEmail = [
                    "emailAddress" => $email["GoremalBean"]["emailAddress"],
                    "gtvemalDesc" => $email["GoremalBean"]["gtvemalDesc"],
                    "emalCode" => $email["GoremalBean"]["emalCode"],
                    "preferredInd" => $email["GoremalBean"]["preferredInd"],
                    "comment" => $email["GoremalBean"]["comment"],
                    "orig_emalCode" => $email["GoremalBean"]["orig_emalCode"],
                    "orig_emailAddress" => $email["GoremalBean"]["orig_emailAddress"],
                ];

                array_push($allEmails, $currentEmail);
            }

            $FinalResponse["Emails"] = $allEmails;

            // echo json_encode($FinalResponse);

            return $response->withStatus(200)
                ->withHeader('Content-type', 'application/json')
                ->write(json_encode($FinalResponse));

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $res = $e->getResponse();
            return $response->withStatus($res->getStatusCode())
                ->write($res->getBody());
        }
    } else {
        return "Please provide a valid JWT token";
    }
});

$app->post("/updateAddress", function ($request, $response) {
    if ($request->hasHeader('Authorization')) {
        require __DIR__ . '/../src/variables.php';
        $data = $request->getBody();

        $data = json_decode($data, true);

        $data['validationOnly'] = "false";
        $data['appNameForOrigin'] = "mylu-ws-mobile";
        $data['user'] = "MYLU-USER";

        $data = json_encode($data);

        $response = postApi($baseUrl . "address/addUpdate", $basicPassword, $data);

        return $response;
    } else {
        return "Please provide a valid JWT token";
    }
});

$app->post("/deleteAddress", function ($request, $response) {
    if ($request->hasHeader('Authorization')) {
        require __DIR__ . '/../src/variables.php';
        $data = $request->getBody();

        $data = json_decode($data, true);

        $data['appNameForOrigin'] = "mylu-ws-mobile";
        $data['user'] = "MYLU-USER";

        $data = json_encode($data);

        $response = postApi($baseUrl . "address/inactivate", $basicPassword, $data);

        return $response;
    } else {
        return "Please provide a valid JWT token";
    }
});

$app->post("/updatePhone", function ($request, $response) {
    if ($request->hasHeader('Authorization')) {
        require __DIR__ . '/../src/variables.php';
        $data = $request->getBody();

        $data = json_decode($data, true);

        $data['validationOnly'] = "false";
        $data['appNameForOrigin'] = "mylu-ws-mobile";
        $data['user'] = "MYLU-USER";

        $data = json_encode($data);

        $response = postApi($baseUrl . "phone/addUpdate", $basicPassword, $data);

        return $response;
    } else {
        return "Please provide a valid JWT token";
    }
});

$app->post("/deletePhone", function ($request, $response) {
    if ($request->hasHeader('Authorization')) {
        require __DIR__ . '/../src/variables.php';
        $data = $request->getBody();

        $data = json_decode($data, true);

        $data['appNameForOrigin'] = "mylu-ws-mobile";
        $data['user'] = "MYLU-USER";

        $data = json_encode($data);

        $response = postApi($baseUrl . "phone/inactivate", $basicPassword, $data);

        return $response;
    } else {
        return "Please provide a valid JWT token";
    }
});

function getApi($url, $basicPassword)
{
    $client = new \GuzzleHttp\Client();

    try {
        $response = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => 'Basic ' . $basicPassword,
            ],
        ]);

        return $responseAsArray = json_decode($response->getBody(), true);

    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
        $response = $e->getResponse();
        return $response;
    }
}

function postApi($url, $basicPassword, $data)
{
    $client = new \GuzzleHttp\Client();

    try {
        $response = $client->request('POST', $url, [
            'headers' => [
                'Authorization' => 'Basic ' . $basicPassword,
            ],
            'body' => $data,
        ]);

        return $response;

    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
        $response = $e->getResponse();
        return $response;
    }
}

$app->run();
